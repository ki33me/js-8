/*
Опишіть своїми словами що таке Document Object Model (DOM)
Структура веб страницы в которой есть своя иерархия обьектов 
Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerHTML содержит всю html структуру элемента включая потомков  : контент  свойства, стили, теги и тд
 innerText содержит ТОЛЬКО контент элемента и потомков
Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? 
у document есть методы такие как :  getElementById, getElementsByClass, getElementsByTagName, querySelector, querySelectorAll
Больше универсала и удобства лично для меня в querySelector, querySelectorAll


-------------

Знайти всі параграфи на сторінці та встановити колір фону #ff0000

Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
This is a paragraph

Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


*/
console.log(document.querySelectorAll("p"));

document.querySelector(".main-header").style.background = "#ff0000";

console.log(document.querySelector('#optionsList').parentElement);
console.log(document.querySelector('#optionsList').childNodes);


let pElement = document.querySelector("#testParagraph").textContent = "This is a paragraph" ;
console.log(pElement);



// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

for (let item of document.querySelector('.main-header').children) {
    item.classList.add('nav-item');
}
console.log(document.querySelector('.main-header').children);

//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


for (let item of document.querySelectorAll('.section-title')) {
    item.classList.remove('section-title');
}
